#!/usr/bin/env bash
	echo "Remember to type your password! This script uses sudo."
# Installing RPM Fusion to the system, both free and non-free repositories:	
	sudo dnf install https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm https://mirrors.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
# Extra dnf configuration, enabling fastest mirror and raising parallel downloads to 10:
	echo 'fastestmirror = true' | sudo tee -a /etc/dnf/dnf.conf && echo 'max_parallel_downloads=10' | sudo tee -a /etc/dnf/dnf.conf
# Refreshing & updating the repositories:	
	sudo dnf update --refresh
# Installing applications:
	sudo dnf install emacs kitty neovim neofetch gnome-tweaks gnome-shell-extension-pop-shell xprop lutris xdelta qbittorrent piper vlc vlc-bittorrent mpv obs-studio kdenlive telegram-desktop zenity openssl libvterm libvterm-devel steam
# Plugins for movies and music:
	sudo dnf install gstreamer1-plugins-{bad-\*,good-\*,base} gstreamer1-plugin-openh264 gstreamer1-libav --exclude=gstreamer1-plugins-bad-free-devel
        sudo dnf install lame\* --exclude=lame-devel
        sudo dnf group upgrade --with-optional Multimedia
# Adding the Flathub repo.
	flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
# Now we install WineHQ, in my case staging, feel free to change the version by editing this script.
	sudo dnf config-manager --add-repo https://dl.winehq.org/wine-builds/fedora/36/winehq.repo
        sudo dnf install winehq-staging
# Getting started with virtualization (via virt-manager):
# Uncomment the following lines install the following dependencies to use virtualization with virt-manager.
#       sudo dnf install @virtualization
#       sudo dnf group install --with-optional virtualization
#       sudo systemctl start libvirtd
#       sudo systemctl enable libvirtd
# Installing VSCodium (For those who might not know, VSCodium is a FOSS project that offers telemetry-free VSCode binaries under the MIT License.):
# First we add the GitLab repo to our yum repos:
        sudo tee -a /etc/yum.repos.d/vscodium.repo << 'EOF'
[gitlab.com_paulcarroty_vscodium_repo]
name=gitlab.com_paulcarroty_vscodium_repo
baseurl=https://paulcarroty.gitlab.io/vscodium-deb-rpm-repo/rpms/
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/master/pub.gpg
metadata_expire=1h
EOF
# Now we install Codium with the following command:
        sudo dnf install codium















