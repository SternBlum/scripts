#!/usr/bin/env bash
    echo "Welcome! This script will install the NIX package manager, once the installation is finished, remember to either log out or reboot your system!"
# The installation itself:
    curl -L https://nixos.org/nix/install | sh
# End of the script
    echo "All done! Time to log out/reboot the system!"